<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StmServers
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.SvrGrid = New System.Windows.Forms.DataGridView
        Me.cAlias = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.StmServer1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.StmPort = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.StmUser = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.StmPwd = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.StmMaxMailPerHour = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PxySvr = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PxyPort = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PxyUser = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PxyPwd = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.SvrGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SvrGrid
        '
        Me.SvrGrid.AllowUserToOrderColumns = True
        Me.SvrGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.SvrGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cAlias, Me.StmServer1, Me.StmPort, Me.StmUser, Me.StmPwd, Me.StmMaxMailPerHour, Me.PxySvr, Me.PxyPort, Me.PxyUser, Me.PxyPwd})
        Me.SvrGrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SvrGrid.Location = New System.Drawing.Point(0, 0)
        Me.SvrGrid.Name = "SvrGrid"
        Me.SvrGrid.Size = New System.Drawing.Size(1047, 466)
        Me.SvrGrid.TabIndex = 0
        '
        'cAlias
        '
        Me.cAlias.HeaderText = "Alias"
        Me.cAlias.Name = "cAlias"
        '
        'StmServer1
        '
        Me.StmServer1.HeaderText = "STM Server"
        Me.StmServer1.Name = "StmServer1"
        '
        'StmPort
        '
        Me.StmPort.HeaderText = "STM Port"
        Me.StmPort.Name = "StmPort"
        '
        'StmUser
        '
        Me.StmUser.HeaderText = "STM User"
        Me.StmUser.Name = "StmUser"
        '
        'StmPwd
        '
        Me.StmPwd.HeaderText = "STM Password"
        Me.StmPwd.Name = "StmPwd"
        '
        'StmMaxMailPerHour
        '
        Me.StmMaxMailPerHour.HeaderText = "Max Mail/Hour"
        Me.StmMaxMailPerHour.Name = "StmMaxMailPerHour"
        '
        'PxySvr
        '
        Me.PxySvr.HeaderText = "Proxy Server"
        Me.PxySvr.Name = "PxySvr"
        '
        'PxyPort
        '
        Me.PxyPort.HeaderText = "Proxy Port"
        Me.PxyPort.Name = "PxyPort"
        '
        'PxyUser
        '
        Me.PxyUser.HeaderText = "Proxy User"
        Me.PxyUser.Name = "PxyUser"
        '
        'PxyPwd
        '
        Me.PxyPwd.HeaderText = "Proxy Password"
        Me.PxyPwd.Name = "PxyPwd"
        '
        'StmServers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1047, 466)
        Me.Controls.Add(Me.SvrGrid)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "StmServers"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "StmServers"
        CType(Me.SvrGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SvrGrid As System.Windows.Forms.DataGridView
    Friend WithEvents cAlias As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StmServer1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StmPort As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StmUser As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StmPwd As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StmMaxMailPerHour As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PxySvr As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PxyPort As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PxyUser As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PxyPwd As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
