<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mailer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Features = New System.Windows.Forms.GroupBox
        Me.btnMessages = New System.Windows.Forms.Button
        Me.btnStmServer = New System.Windows.Forms.Button
        Me.tmrGlobal = New System.Windows.Forms.Timer(Me.components)
        Me.Features.SuspendLayout()
        Me.SuspendLayout()
        '
        'Features
        '
        Me.Features.Controls.Add(Me.btnMessages)
        Me.Features.Controls.Add(Me.btnStmServer)
        Me.Features.Location = New System.Drawing.Point(21, 32)
        Me.Features.Name = "Features"
        Me.Features.Size = New System.Drawing.Size(672, 401)
        Me.Features.TabIndex = 0
        Me.Features.TabStop = False
        Me.Features.Text = "     Features    "
        '
        'btnMessages
        '
        Me.btnMessages.Location = New System.Drawing.Point(110, 44)
        Me.btnMessages.Name = "btnMessages"
        Me.btnMessages.Size = New System.Drawing.Size(75, 23)
        Me.btnMessages.TabIndex = 2
        Me.btnMessages.Text = "&Messages"
        Me.btnMessages.UseVisualStyleBackColor = True
        '
        'btnStmServer
        '
        Me.btnStmServer.Location = New System.Drawing.Point(29, 44)
        Me.btnStmServer.Name = "btnStmServer"
        Me.btnStmServer.Size = New System.Drawing.Size(75, 23)
        Me.btnStmServer.TabIndex = 0
        Me.btnStmServer.Text = "&STM Server"
        Me.btnStmServer.UseVisualStyleBackColor = True
        '
        'tmrGlobal
        '
        Me.tmrGlobal.Interval = 1000
        '
        'Mailer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(719, 472)
        Me.Controls.Add(Me.Features)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.Name = "Mailer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mkt Mailer 0.1.1"
        Me.Features.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Features As System.Windows.Forms.GroupBox
    Friend WithEvents btnMessages As System.Windows.Forms.Button
    Friend WithEvents btnStmServer As System.Windows.Forms.Button
    Friend WithEvents tmrGlobal As System.Windows.Forms.Timer

End Class
