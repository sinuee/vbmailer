
Public Class Mailer
    Private trd() As System.Threading.Thread
    Private iCountTrd As Integer
    Private Sub Mailer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ReadServerFromFile()
        iCountTrd = 0
        'LoadContacts()
    End Sub

    Private Sub btnStmServer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStmServer.Click
        StmServers.ShowDialog()
    End Sub

    Private Sub btnMessages_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMessages.Click
        Messages.ShowDialog()
    End Sub

    Private Sub ReadServerFromFile()
        Dim Cont As Integer = 0
        Dim FILE_NAME As String = "C:\Mailer\Servers.cvs"
        Dim TextLine As String = ""
        Dim cadenas As String()

        ReDim Globals.StmServer(0)

        If System.IO.File.Exists(FILE_NAME) Then
            Dim objReader As New System.IO.StreamReader(FILE_NAME)
            Do While objReader.Peek() <> -1
                TextLine = objReader.ReadLine() & vbNewLine
                cadenas = TextLine.Split(",")
                If cadenas.Length >= 9 Then
                    ReDim Preserve Globals.StmServer(Cont)
                    Globals.StmServer(Cont) = New Server
                    Globals.StmServer(Cont).StmSvr.sAlias = cadenas(0)
                    Globals.StmServer(Cont).StmSvr.Server = cadenas(1)
                    Globals.StmServer(Cont).StmSvr.Port = cadenas(2)
                    Globals.StmServer(Cont).StmSvr.User = cadenas(3)
                    Globals.StmServer(Cont).StmSvr.Password = cadenas(4)
                    If IsNumeric(cadenas(5)) Then
                        Globals.StmServer(Cont).StmSvr.MaxMailPerHour = CInt(cadenas(5))
                    Else
                        Globals.StmServer(Cont).StmSvr.MaxMailPerHour = 0
                    End If
                    Globals.StmServer(Cont).PxySvr.Server = cadenas(6)
                    Globals.StmServer(Cont).PxySvr.Port = cadenas(7)
                    Globals.StmServer(Cont).PxySvr.User = cadenas(8)
                    Globals.StmServer(Cont).PxySvr.Password = cadenas(9)
                    Cont = Cont + 1
                End If
            Loop
            objReader.Close()
        Else
            MsgBox("File Does Not Exist")
        End If
    End Sub
    Private Sub tmrGlobal_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrGlobal.Tick
        Ticks = Ticks + 1
        If Ticks > 3600 Then
            Ticks = 0
            ResetServers()
        End If
    End Sub
    Public Sub NewThread(ByVal Mess As cMsg)
        iCountTrd = iCountTrd + 1
        ReDim Preserve trd(iCountTrd)
        trd(iCountTrd) = New System.Threading.Thread(AddressOf Sending)
        trd(iCountTrd).IsBackground = True
        trd(iCountTrd).Start(Mess)
    End Sub
    Public Sub Sending(ByVal Mess As Object)
        Dim DestMail As String
        Dim ActualSvr As Server

        For Each DestMail In Mess.Recipents
            For Each ActualSvr In Globals.StmServer
                If ActualSvr.Active Then
                    If ActualSvr.CheckLimit Then
                        SentMail.SentMail(Mess.ReplyTo, Mess.from, DestMail, _
                                    Mess.Subj, Mess.Body, ActualSvr.StmSvr.Server, _
                                    ActualSvr.StmSvr.User, ActualSvr.StmSvr.Password, _
                                    ActualSvr.StmSvr.Port, Mess.Attachs(0))
                        ActualSvr.iMailSentHour = ActualSvr.iMailSentHour + 1
                    Else
                        ActualSvr.Active = False
                    End If
                End If
            Next
        Next
    End Sub
    Private Sub ResetServers()
        Dim svr As Server
        For Each svr In Globals.StmServer
            svr.Active = True
            svr.iMailSentHour = 0
        Next
    End Sub
End Class

