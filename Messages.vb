Public Class Messages
    Public Msg As cMsg

    Private Sub btnAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAttach.Click
        AttachFiles.ShowDialog()
        Dim i As Integer = 0
        lstVwAttached.Clear()
        For i = 0 To Msg.Attachs.Length - 1
            lstVwAttached.Items.Add(Msg.Attachs(i))
        Next
    End Sub

    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Dim mTo As String = txtTo.Text
        Dim iSvr As Integer = cmbFrom.SelectedIndex
        PassListToArray()
        FillMessage()
        Mailer.NewThread(Msg)
        Clear()
        Me.Close()
    End Sub

    Private Sub btnDiscard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDiscard.Click
        Clear()
        Me.Close()
    End Sub

    Private Sub Messages_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        AttachFiles.lstVwAttached.Items.Clear()
        lstVwAttached.Items.Clear()
    End Sub

    Private Sub Messages_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Msg = New cMsg
        fillCmbFrom()
    End Sub
    Private Sub fillCmbFrom()
        Dim i As Integer = 0
        Dim Svr As Server
        If Globals.StmServer(0) IsNot Nothing Then
            cmbFrom.Items.Clear()
            For Each Svr In Globals.StmServer
                If Globals.StmServer(i) IsNot Nothing Then
                    cmbFrom.Items.Insert(i, Svr.StmSvr.sAlias)
                    i = i + 1
                End If
            Next
        End If
    End Sub

    Private Sub BuildToFromFile(ByRef sTo As String)
        Dim id As Conctac
        sTo = ""
        For Each id In aContacs
            sTo = sTo & id.mail & ","
        Next
        ' sTo.CopyTo(sTo, sTo, 0, sTo.Length - 1)
    End Sub
    Private Sub PassListToArray()
        Dim i As Integer = 0
        ReDim Msg.Attachs(lstVwAttached.Items.Count)
        For i = 0 To lstVwAttached.Items.Count - 1
            Msg.Attachs(i) = lstVwAttached.Items(i).Text
        Next
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        PassListToArray()
        FillMessage()
        Dim FileToSave As New SaveFileDialog
        FileToSave.Filter = "Mensaje Personalizado(*.zMsg)|*.zMsg"
        FileToSave.InitialDirectory = "C:\Mailer"
        If Not System.IO.Directory.Exists("C:\Mailer") Then
            System.IO.Directory.CreateDirectory("C:\Mailer")
        End If
        FileToSave.AddExtension = True
        FileToSave.ShowDialog()
        Dim filePath As String = FileToSave.FileName
        Dim ObjWrite As New System.IO.StreamWriter(filePath)
        Dim x As New Xml.Serialization.XmlSerializer(Msg.GetType())
        x.Serialize(ObjWrite, Msg)
        ObjWrite.Close()
    End Sub

    Private Sub btnRecipentFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecipentFile.Click
        Dim fd As OpenFileDialog = New OpenFileDialog()
        Dim sPath As String
        Dim sTo As String = ""
        sPath = fd.ShowDialog()
        LoadContacts(sPath)
        BuildToFromFile(sTo)
        txtTo.Text = sTo
    End Sub
    Private Sub LoadContacts(ByVal FILE_NAME As String)
        Dim TextLine As String = ""
        Dim cadenas As String()
        Dim Cont As Integer = 0
        Dim i As Integer = 0

        ReDim Globals.aContacs(0)

        If System.IO.File.Exists(FILE_NAME) Then
            Dim ObjReader As New System.IO.StreamReader(FILE_NAME)
            Do While ObjReader.Peek() <> -1
                TextLine = ObjReader.ReadLine() & vbNewLine
                cadenas = TextLine.Split(",")
                If cadenas.Length > 2 Then
                    If IsNumeric(cadenas(0)) Then
                        ReDim Preserve aContacs(Cont)
                        aContacs(Cont) = New Conctac
                        aContacs(Cont).SetId(CInt(cadenas(0)))
                        aContacs(Cont).name = cadenas(1)
                        aContacs(Cont).mail = cadenas(2)
                    End If

                End If
                Cont = Cont + 1
            Loop
            If Cont = 0 Then
                ReDim aContacs(0)
                aContacs(0) = New Conctac
            End If
            ObjReader.Close()
        Else
            Dim f As System.IO.FileStream
            f = System.IO.File.Create(FILE_NAME)
            f.Close()
            ReDim Preserve aContacs(0)
            aContacs(0) = New Conctac
        End If

    End Sub
    Private Sub FillMessage()
        Msg.ReplyTo = txtReply.Text
        Msg.from = TextBox2.Text
        Msg.Recipents = txtTo.Text.Split(",")
        Msg.Subj = txtSubject.Text
        Msg.Body = txtMessage.Text
    End Sub

    Private Sub btnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen.Click
        Dim FileToOpen As New OpenFileDialog
        Dim Path As String
        Clear()
        FileToOpen.Filter = "Mensaje Personalizado(*.zMsg)|*.zMsg"
        FileToOpen.InitialDirectory = "C:\Mailer"
        If Not System.IO.Directory.Exists("C:\Mailer") Then
            System.IO.Directory.CreateDirectory("C:\Mailer")
        End If
        If FileToOpen.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Path = FileToOpen.FileName
            If System.IO.File.Exists(Path) Then
                Dim ObjRead As New System.IO.StreamReader(Path)
                Dim x As New Xml.Serialization.XmlSerializer(Msg.GetType())
                Dim xmsg As cMsg
                xmsg = x.Deserialize(ObjRead)
                ObjRead.Close()
                fillGUI(xmsg)
            End If
        End If
    End Sub
    Private Sub fillGUI(ByVal x As cMsg)
        Dim sTo As String = ""
        Dim i As Integer = 0
        txtMessage.Text = x.Body
        txtReply.Text = x.ReplyTo
        txtSubject.Text = x.Subj
        TextBox2.Text = x.from
        If Not x.Recipents(0) = Nothing Then
            ReDim aContacs(x.Recipents.Length - 1)
            For i = 0 To x.Recipents.Length - 1
                aContacs(i) = New Conctac
                aContacs(i).mail = x.Recipents(i)
            Next
            BuildToFromFile(sTo)
            txtTo.Text = sTo
        End If
        lstVwAttached.Clear()
        For i = 0 To Msg.Attachs.Length - 1
            If Not x.Attachs(i) = Nothing Then
                lstVwAttached.Items.Add(x.Attachs(i))
            End If
        Next
    End Sub
    Private Sub Clear()
        txtMessage.Text = ""
        txtReply.Text = ""
        txtSubject.Text = ""
        TextBox2.Text = ""
        txtTo.Text = ""
        lstVwAttached.Clear()
        Msg = New cMsg
    End Sub
End Class