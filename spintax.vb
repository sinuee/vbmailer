Imports System
Imports System.Text.RegularExpressions
Module spintax
    Public Function spintaxParse(ByVal rand As Random, ByVal s As String) As String
        If (s.Contains("{")) Then
            Dim closingBracePosition As Integer = s.IndexOf("}")
            Dim openingBracePosition As Integer = closingBracePosition

            While (Not s(openingBracePosition).Equals("{"))
                openingBracePosition = openingBracePosition - 1
            End While

            Dim spintaxBlock As String = s.Substring(openingBracePosition, closingBracePosition - openingBracePosition + 1)

            Dim items() As String = spintaxBlock.Substring(1, spintaxBlock.Length - 2).Split("|")

            s = s.Replace(spintaxBlock, items(rand.Next(items.Length)))

            Return spintaxParse(rand, s)
        Else
            Return s
        End If
    End Function
    Public Function spintax(ByVal rnd As Random, ByVal str As String) As String
        ' Loop over string until all patterns exhausted.
        Dim pattern As String = "{[^{}]*}"
        Dim m As Match = Regex.Match(str, pattern)
        While (m.Success)
            ' Get random choice and replace pattern match.
            Dim seg As String = str.Substring(m.Index + 1, m.Length - 2)
            Dim choices() As String = seg.Split("|")
            str = str.Substring(0, m.Index) + choices(rnd.Next(choices.Length)) + str.Substring(m.Index + m.Length)
            m = Regex.Match(str, pattern)
        End While

        ' Return the modified string.
        Return str
    End Function
End Module
