Public Class AttachFiles

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim i As Integer = 0
        ReDim Messages.Msg.Attachs(lstVwAttached.Items.Count)
        For i = 0 To lstVwAttached.Items.Count - 1
            Messages.Msg.Attachs(i) = lstVwAttached.Items(0).Text
        Next
        Me.Close()
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        Dim fd As OpenFileDialog = New OpenFileDialog()

        fd.Title = "Open File Dialog"
        fd.InitialDirectory = "C:\"
        fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        fd.FilterIndex = 2
        fd.RestoreDirectory = True

        If fd.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            lstVwAttached.Items.Add(fd.FileName)
        End If
    End Sub
End Class