Public Class STM
    Public sAlias As String
    Public Server As String
    Public Port As String
    Public User As String
    Public Password As String
    Public MaxMailPerHour As Integer
    Public MailsSended As Integer
    Public Sub New()
        sAlias = "A"
        Server = ""
        Port = "25"
        User = ""
        Password = ""
        MaxMailPerHour = 0 ' 0 es ilimitado
        MailsSended = 0
    End Sub
    Shared Operator =(ByVal ValD As STM, ByVal ValI As STM) As Boolean
        If ValD.sAlias = ValI.sAlias And ValD.Server = ValI.Server And ValD.Port = ValI.Port And ValD.User = ValI.User And ValD.Password = ValI.Password Then
            Return True
        Else
            Return False
        End If
    End Operator
    Shared Operator <>(ByVal ValD As STM, ByVal ValI As STM) As Boolean
        If ValD = ValI Then
            Return False
        Else
            Return True
        End If
    End Operator
End Class
