Public Class Proxy
    Public Server As String
    Public Port As String
    Public User As String
    Public Password As String
    Public Sub New()
        Server = ""
        Port = ""
        User = ""
        Password = ""
    End Sub
    Shared Operator =(ByVal ValD As Proxy, ByVal ValI As Proxy) As Boolean
        If ValD.Server = ValI.Server And ValD.Port = ValI.Port And ValD.User = ValI.User And ValD.Password = ValI.Password Then
            Return True
        Else
            Return False
        End If
    End Operator
    Shared Operator <>(ByVal ValD As Proxy, ByVal ValI As Proxy) As Boolean
        If ValD = ValI Then
            Return False
        Else
            Return True
        End If
    End Operator
End Class
