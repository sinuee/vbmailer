Option Explicit On

Imports System.Net.Mail
Module SentMail
    Public Sub SentMail(ByVal mReplyTo As String, ByVal mFrom As String, ByVal mTo As String, _
    ByVal mSubject As String, ByVal mMessage As String, ByVal host As String, ByVal User As String, ByVal pwd As String, ByVal iPort As Integer, ByRef AttachedFiles As String)
        Dim eMail As New MailMessage
        'Dim uCC As String
        'Dim uCCO As String
        Dim AttachmentFile As String
        Dim Rand As New Random()
        'reply to
        eMail.ReplyTo = New MailAddress(mReplyTo)
        'de
        eMail.From = New MailAddress(mFrom)
        'para
        eMail.To.Add(New MailAddress(mTo))
        'Asunto
        eMail.Subject = spintax.spintax(Rand, mSubject)
        'Cuerpo del correo
        eMail.Body = spintax.spintax(Rand, mMessage)
        'Mostrar como HTML
        eMail.IsBodyHtml = True
        'Prioridad de el correo
        eMail.Priority = MailPriority.High
        'Attached Files
        If AttachedFiles IsNot Nothing Then
            For Each AttachmentFile In AttachedFiles
                If System.IO.File.Exists(AttachmentFile) Then
                    eMail.Attachments.Add(New System.Net.Mail.Attachment(AttachmentFile))
                End If
            Next
        End If

        'enviar
        Dim smtp As New SmtpClient()

        smtp.Host = host
        smtp.Port = iPort
        smtp.Credentials = New System.Net.NetworkCredential(User, pwd)
        smtp.EnableSsl = True
        smtp.DeliveryMethod = SmtpDeliveryMethod.Network


        Try
            smtp.Send(eMail)
        Catch ex As Exception
            MsgBox("ERROR: " & ex.Message)
        End Try


    End Sub
End Module
