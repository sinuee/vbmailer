Public Class cMsg
    Private sRecipents() As String
    Private sBody As String
    Private sSubj As String
    Private sFrom As String
    Private sReplyTo As String
    Private sAttachs() As String
    Property ReplyTo() As String
        Get
            Return sReplyTo
        End Get
        Set(ByVal value As String)
            sReplyTo = value
        End Set
    End Property
    Property Recipents() As String()
        Get
            Return sRecipents
        End Get
        Set(ByVal value() As String)
            sRecipents = value
        End Set
    End Property
    Public Property Recipents(ByVal i As Integer) As Object
        Get
            Return sRecipents(i)
        End Get
        Set(ByVal value As Object)
            sRecipents(i) = value
        End Set
    End Property
    Property Body() As String
        Get
            Return sBody
        End Get
        Set(ByVal value As String)
            sBody = value
        End Set
    End Property
    Property Subj() As String
        Get
            Return sSubj
        End Get
        Set(ByVal value As String)
            sSubj = value
        End Set
    End Property
    Property from() As String
        Get
            Return sFrom
        End Get
        Set(ByVal value As String)
            sFrom = value
        End Set
    End Property
    Property Attachs() As String()
        Get
            Return sAttachs
        End Get
        Set(ByVal value() As String)
            sAttachs = value
        End Set
    End Property
    Property Attachs(ByVal i As Integer) As Object
        Get
            Return sAttachs(i)
        End Get
        Set(ByVal value As Object)
            sAttachs(i) = value
        End Set
    End Property
    Public Sub New()
        ReDim sRecipents(0)
        sRecipents(0) = ""
        sBody = ""
        sSubj = ""
        sFrom = ""
        ReDim sAttachs(0)
        sAttachs(0) = ""
    End Sub
End Class
