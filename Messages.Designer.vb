<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Messages
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblFrom = New System.Windows.Forms.Label
        Me.cmbFrom = New System.Windows.Forms.ComboBox
        Me.lbSubject = New System.Windows.Forms.Label
        Me.txtSubject = New System.Windows.Forms.TextBox
        Me.lblMessage = New System.Windows.Forms.Label
        Me.txtMessage = New System.Windows.Forms.RichTextBox
        Me.btnAttach = New System.Windows.Forms.Button
        Me.btnSend = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnDiscard = New System.Windows.Forms.Button
        Me.lstVwAttached = New System.Windows.Forms.ListView
        Me.txtTo = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.gpTo = New System.Windows.Forms.GroupBox
        Me.btnRecipentFile = New System.Windows.Forms.Button
        Me.lblReply = New System.Windows.Forms.Label
        Me.txtReply = New System.Windows.Forms.TextBox
        Me.btnOpen = New System.Windows.Forms.Button
        Me.gpTo.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        Me.lblFrom.Location = New System.Drawing.Point(32, 37)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(30, 13)
        Me.lblFrom.TabIndex = 0
        Me.lblFrom.Text = "From"
        '
        'cmbFrom
        '
        Me.cmbFrom.FormattingEnabled = True
        Me.cmbFrom.Location = New System.Drawing.Point(330, 9)
        Me.cmbFrom.Name = "cmbFrom"
        Me.cmbFrom.Size = New System.Drawing.Size(192, 21)
        Me.cmbFrom.TabIndex = 1
        '
        'lbSubject
        '
        Me.lbSubject.AutoSize = True
        Me.lbSubject.Location = New System.Drawing.Point(12, 132)
        Me.lbSubject.Name = "lbSubject"
        Me.lbSubject.Size = New System.Drawing.Size(43, 13)
        Me.lbSubject.TabIndex = 4
        Me.lbSubject.Text = "Subject"
        '
        'txtSubject
        '
        Me.txtSubject.Location = New System.Drawing.Point(69, 129)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(191, 20)
        Me.txtSubject.TabIndex = 5
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Location = New System.Drawing.Point(12, 180)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(50, 13)
        Me.lblMessage.TabIndex = 6
        Me.lblMessage.Text = "Message"
        '
        'txtMessage
        '
        Me.txtMessage.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.txtMessage.Location = New System.Drawing.Point(0, 208)
        Me.txtMessage.Name = "txtMessage"
        Me.txtMessage.Size = New System.Drawing.Size(534, 260)
        Me.txtMessage.TabIndex = 7
        Me.txtMessage.Text = ""
        '
        'btnAttach
        '
        Me.btnAttach.Location = New System.Drawing.Point(296, 33)
        Me.btnAttach.Name = "btnAttach"
        Me.btnAttach.Size = New System.Drawing.Size(226, 21)
        Me.btnAttach.TabIndex = 8
        Me.btnAttach.Text = "Attach File"
        Me.btnAttach.UseVisualStyleBackColor = True
        '
        'btnSend
        '
        Me.btnSend.Location = New System.Drawing.Point(427, 172)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(98, 28)
        Me.btnSend.TabIndex = 9
        Me.btnSend.Text = "&Send"
        Me.btnSend.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(323, 172)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(98, 28)
        Me.btnSave.TabIndex = 10
        Me.btnSave.Text = "S&ave"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnDiscard
        '
        Me.btnDiscard.Location = New System.Drawing.Point(219, 172)
        Me.btnDiscard.Name = "btnDiscard"
        Me.btnDiscard.Size = New System.Drawing.Size(98, 28)
        Me.btnDiscard.TabIndex = 11
        Me.btnDiscard.Text = "&Discard"
        Me.btnDiscard.UseVisualStyleBackColor = True
        '
        'lstVwAttached
        '
        Me.lstVwAttached.AutoArrange = False
        Me.lstVwAttached.GridLines = True
        Me.lstVwAttached.LabelWrap = False
        Me.lstVwAttached.Location = New System.Drawing.Point(296, 60)
        Me.lstVwAttached.Name = "lstVwAttached"
        Me.lstVwAttached.Size = New System.Drawing.Size(226, 68)
        Me.lstVwAttached.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lstVwAttached.TabIndex = 12
        Me.lstVwAttached.UseCompatibleStateImageBehavior = False
        '
        'txtTo
        '
        Me.txtTo.Location = New System.Drawing.Point(9, 19)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(191, 20)
        Me.txtTo.TabIndex = 13
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(69, 33)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(192, 20)
        Me.TextBox2.TabIndex = 14
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(261, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Stm Server"
        '
        'gpTo
        '
        Me.gpTo.Controls.Add(Me.btnRecipentFile)
        Me.gpTo.Controls.Add(Me.txtTo)
        Me.gpTo.Location = New System.Drawing.Point(6, 59)
        Me.gpTo.Name = "gpTo"
        Me.gpTo.Size = New System.Drawing.Size(255, 52)
        Me.gpTo.TabIndex = 16
        Me.gpTo.TabStop = False
        Me.gpTo.Text = "    To   "
        '
        'btnRecipentFile
        '
        Me.btnRecipentFile.Location = New System.Drawing.Point(208, 18)
        Me.btnRecipentFile.Name = "btnRecipentFile"
        Me.btnRecipentFile.Size = New System.Drawing.Size(31, 21)
        Me.btnRecipentFile.TabIndex = 14
        Me.btnRecipentFile.Text = "..."
        Me.btnRecipentFile.UseVisualStyleBackColor = True
        '
        'lblReply
        '
        Me.lblReply.AutoSize = True
        Me.lblReply.Location = New System.Drawing.Point(12, 9)
        Me.lblReply.Name = "lblReply"
        Me.lblReply.Size = New System.Drawing.Size(50, 13)
        Me.lblReply.TabIndex = 17
        Me.lblReply.Text = "Reply To"
        '
        'txtReply
        '
        Me.txtReply.Location = New System.Drawing.Point(69, 6)
        Me.txtReply.Name = "txtReply"
        Me.txtReply.Size = New System.Drawing.Size(191, 20)
        Me.txtReply.TabIndex = 18
        '
        'btnOpen
        '
        Me.btnOpen.Location = New System.Drawing.Point(109, 172)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(98, 28)
        Me.btnOpen.TabIndex = 19
        Me.btnOpen.Text = "&Open"
        Me.btnOpen.UseVisualStyleBackColor = True
        '
        'Messages
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(534, 468)
        Me.Controls.Add(Me.btnOpen)
        Me.Controls.Add(Me.txtReply)
        Me.Controls.Add(Me.lblReply)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.lstVwAttached)
        Me.Controls.Add(Me.btnDiscard)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnSend)
        Me.Controls.Add(Me.btnAttach)
        Me.Controls.Add(Me.txtMessage)
        Me.Controls.Add(Me.lblMessage)
        Me.Controls.Add(Me.txtSubject)
        Me.Controls.Add(Me.lbSubject)
        Me.Controls.Add(Me.cmbFrom)
        Me.Controls.Add(Me.lblFrom)
        Me.Controls.Add(Me.gpTo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "Messages"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Messages"
        Me.gpTo.ResumeLayout(False)
        Me.gpTo.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents cmbFrom As System.Windows.Forms.ComboBox
    Friend WithEvents lbSubject As System.Windows.Forms.Label
    Friend WithEvents txtSubject As System.Windows.Forms.TextBox
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents txtMessage As System.Windows.Forms.RichTextBox
    Friend WithEvents btnAttach As System.Windows.Forms.Button
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnDiscard As System.Windows.Forms.Button
    Friend WithEvents lstVwAttached As System.Windows.Forms.ListView
    Friend WithEvents txtTo As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents gpTo As System.Windows.Forms.GroupBox
    Friend WithEvents btnRecipentFile As System.Windows.Forms.Button
    Friend WithEvents lblReply As System.Windows.Forms.Label
    Friend WithEvents txtReply As System.Windows.Forms.TextBox
    Friend WithEvents btnOpen As System.Windows.Forms.Button
End Class
