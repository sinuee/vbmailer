Public Class StmServers
    Private Sub StmServers_FillServersGrid()
        Dim Svr As Server
        Dim i As Integer = 0
        SvrGrid.Rows.Clear()
        SvrGrid.Rows.Add(Globals.StmServer.Length)
        For Each Svr In Globals.StmServer
            If Svr IsNot Nothing Then
                SvrGrid.Rows(i).Cells(0).Value = Svr.StmSvr.sAlias
                SvrGrid.Rows(i).Cells(1).Value = Svr.StmSvr.Server
                SvrGrid.Rows(i).Cells(2).Value = Svr.StmSvr.Port
                SvrGrid.Rows(i).Cells(3).Value = Svr.StmSvr.User
                SvrGrid.Rows(i).Cells(4).Value = Svr.StmSvr.Password
                SvrGrid.Rows(i).Cells(5).Value = Svr.StmSvr.MaxMailPerHour.ToString()
                SvrGrid.Rows(i).Cells(6).Value = Svr.PxySvr.Server
                SvrGrid.Rows(i).Cells(7).Value = Svr.PxySvr.Port
                SvrGrid.Rows(i).Cells(8).Value = Svr.PxySvr.User
                SvrGrid.Rows(i).Cells(9).Value = Svr.PxySvr.Password
                i = i + 1
            End If
        Next
    End Sub
    Private Sub StmServers_UpdateServers()
        Dim i As Integer = 0
        ReDim Globals.StmServer(0)
        For i = 0 To SvrGrid.Rows.Count - 2
            ReDim Preserve Globals.StmServer(i)
            Globals.StmServer(i) = New Server
            Globals.StmServer(i).StmSvr.sAlias = SvrGrid.Rows(i).Cells(0).Value
            Globals.StmServer(i).StmSvr.Server = SvrGrid.Rows(i).Cells(1).Value
            Globals.StmServer(i).StmSvr.Port = SvrGrid.Rows(i).Cells(2).Value
            Globals.StmServer(i).StmSvr.User = SvrGrid.Rows(i).Cells(3).Value
            Globals.StmServer(i).StmSvr.Password = SvrGrid.Rows(i).Cells(4).Value
            Globals.StmServer(i).StmSvr.MaxMailPerHour = SvrGrid.Rows(i).Cells(5).Value
            Globals.StmServer(i).PxySvr.Server = SvrGrid.Rows(i).Cells(6).Value
            Globals.StmServer(i).PxySvr.Port = SvrGrid.Rows(i).Cells(7).Value
            Globals.StmServer(i).PxySvr.User = SvrGrid.Rows(i).Cells(8).Value
            Globals.StmServer(i).PxySvr.Password = SvrGrid.Rows(i).Cells(9).Value
        Next
    End Sub
    Private Sub StmServers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        StmServers_FillServersGrid()
    End Sub

    Private Sub StmServers_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        StmServers_UpdateServers()
        PreservServers()
    End Sub
    Private Sub PreservServers()
        Dim FILE_NAME As String = "C:\Mailer\Servers.cvs"
        Dim TextLine As String = ""
        Dim Cont As Integer = 0

        If System.IO.File.Exists(FILE_NAME) = True Then
            System.IO.File.Delete(FILE_NAME)
            'System.IO.File.Create(FILE_NAME)

        Else
            'System.IO.File.Create(FILE_NAME)
        End If

        Dim ObjWrite As New System.IO.StreamWriter(FILE_NAME)
        For Cont = 0 To Globals.StmServer.Length - 1
            TextLine = Globals.StmServer(Cont).StmSvr.sAlias & ","
            TextLine = TextLine & Globals.StmServer(Cont).StmSvr.Server & ","
            TextLine = TextLine & Globals.StmServer(Cont).StmSvr.Port & ","
            TextLine = TextLine & Globals.StmServer(Cont).StmSvr.User & ","
            TextLine = TextLine & Globals.StmServer(Cont).StmSvr.Password & ","
            TextLine = TextLine & Globals.StmServer(Cont).StmSvr.MaxMailPerHour & ","
            TextLine = TextLine & Globals.StmServer(Cont).PxySvr.Server & ","
            TextLine = TextLine & Globals.StmServer(Cont).PxySvr.Port & ","
            TextLine = TextLine & Globals.StmServer(Cont).PxySvr.User & ","
            TextLine = TextLine & Globals.StmServer(Cont).PxySvr.Password & ","
            ObjWrite.WriteLine(TextLine & vbNewLine)
        Next
        ObjWrite.Close()

    End Sub
End Class