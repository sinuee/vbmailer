Public Class Server
    Public StmSvr As STM
    Public PxySvr As Proxy
    Private MailSentHour As Integer
    Private bActive As Boolean
    Property Active() As Boolean
        Get
            Return bActive
        End Get
        Set(ByVal value As Boolean)
            bActive = value
        End Set
    End Property
    Property iMailSentHour() As Integer
        Get
            Return MailSentHour
        End Get
        Set(ByVal value As Integer)
            MailSentHour = value
        End Set
    End Property
    Public Sub New()
        StmSvr = New STM
        PxySvr = New Proxy
        MailSentHour = 0
        bActive = True
    End Sub
    Shared Operator =(ByVal ValD As Server, ByVal ValI As Server) As Boolean
        If ValD.StmSvr = ValI.StmSvr And ValD.PxySvr = ValI.PxySvr Then
            Return True
        Else
            Return False
        End If
    End Operator
    Shared Operator <>(ByVal ValD As Server, ByVal ValI As Server) As Boolean
        If ValD = ValI Then
            Return False
        Else
            Return True
        End If
    End Operator
    Public Function CheckLimit() As Boolean
        Return MailSentHour < StmSvr.MaxMailPerHour
    End Function
End Class
